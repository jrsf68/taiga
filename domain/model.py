from sqlalchemy import Column, DateTime, func, String
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Project(Base):
    __tablename__ = 'project'

    _id = Column(String(25), primary_key=True)
    insert_at = Column(DateTime)
    tool_id = Column(String(255), unique=True, nullable=False)
    tool = Column(String(255), nullable=False)
    id = Column(String(25), nullable=False)
    name = Column(String(255), nullable=False)
    description = Column(String(255))
    modified_date = Column(DateTime)

    def __repl__(self):
        return f'tool: {self.tool}, id: {self.id}, name: {self.name}'
