[
    {
        'id': '1918c814-d70c-11ee-8e02-2912dd486019', 
        'user': {'pk': 17, 'username': 'Ronald_Almeida', 'name': 'Ronald José Gama Almeida', 'photo': None, 'is_active': True, 'gravatar_id': '6635fcc0fe7a232f75b159e215909778'}, 
        'created_at': '2024-02-29T14:09:05.569Z', 
        'type': 1, 
        'key': 'tasks.task:177', 
        'diff': {}, 
        'snapshot': None, 
        'values': {'users': {} }, 
        'values_diff': {}, 
        'comment': 'Experimentação para implementação da separação do módulo web em partes, administrativa e pública, testes e compatibilidades feitas e aguardando liberação para implementação. A implementação espera o aval para ser realizada, logo após o conserto de bugs no módulo.\n\n*   [x] Separação da área administrativa da área pública;\n*   [x] Separação da área pública da área administrativa;\n*   [x] Manutenção de rotas e domínios;\n*   [x] Limpeza de código e refatoração;', 
        'comment_html': '<p>Experimentação para implementação da separação do módulo web em partes, administrativa e pública, testes e compatibilidades feitas e aguardando liberação para implementação. A implementação espera o aval para ser realizada, logo após o conserto de bugs no módulo.</p>\n<ul class="task-list">\n<li class="task-list-item"><label class="task-list-control"><input checked type="checkbox"><span class="task-list-indicator"></span></label> Separação da área administrativa da área pública;</li>\n<li class="task-list-item"><label class="task-list-control"><input checked type="checkbox"><span class="task-list-indicator"></span></label> Separação da área pública da área administrativa;</li>\n<li class="task-list-item"><label class="task-list-control"><input checked type="checkbox"><span class="task-list-indicator"></span></label> Manutenção de rotas e domínios;</li>\n<li class="task-list-item"><label class="task-list-control"><input checked type="checkbox"><span class="task-list-indicator"></span></label> Limpeza de código e refatoração;</li>\n</ul>', 
        'delete_comment_date': None, 
        'delete_comment_user': None, 
        'edit_comment_date': '2024-02-29T14:13:46.783Z', 
        'is_hidden': False, 
        'is_snapshot': False
    },
    {
        'id': 'b0ef77dc-d6fd-11ee-8e02-2912dd486019', 
        'user': {'pk': 10, 'username': 'Adriana', 'name': 'Adriana Teles', 'photo': None, 'is_active': True, 'gravatar_id': 'b5dfe9fc6086b5e9c2cc22a22280d5ad'}, 
        'created_at': '2024-02-29T12:25:57.861Z', 
        'type': 1, 
        'key': 'tasks.task:177', 
        'diff': {'status': [72, 73], 'taskboard_order': [1708960241437, 0]}, 
        'snapshot': None, 
        'values': {'users': {}, 'status': {'72': 'New', '73': 'In progress'} }, 
        'values_diff': {'status': ['New', 'In progress'], 'taskboard_order': [1708960241437, 0]}, 
        'comment': '', 
        'comment_html': '', 
        'delete_comment_date': None, 
        'delete_comment_user': None, 
        'edit_comment_date': None, 
        'is_hidden': False, 
        'is_snapshot': False
    }, 
    {
        'id': '38bb5b5a-d4b9-11ee-8e02-2912dd486019', 
        'user': {'pk': 10, 'username': 'Adriana', 'name': 'Adriana Teles', 'photo': None, 'is_active': True, 'gravatar_id': 'b5dfe9fc6086b5e9c2cc22a22280d5ad'}, 
        'created_at': '2024-02-26T15:10:48.090Z', 
        'type': 1, 
        'key': 'tasks.task:177', 
        'diff': {'assigned_to': [None, 17]}, 
        'snapshot': None, 
        'values': {'users': {'17': 'Ronald José Gama Almeida'} }, 
        'values_diff': {'assigned_to': [None, 'Ronald José Gama Almeida']}, 
        'comment': '', 
        'comment_html': '', 
        'delete_comment_date': None, 
        'delete_comment_user': None, 
        'edit_comment_date': None, 
        'is_hidden': False, 
        'is_snapshot': False
    }
]








[
    {
        'id': '1918c814-d70c-11ee-8e02-2912dd486019', 
        'user': {'pk': 17, 'username': 'Ronald_Almeida', 'name': 'Ronald José Gama Almeida', 'photo': None, 'is_active': True, 'gravatar_id': '6635fcc0fe7a232f75b159e215909778'}, 
        'created_at': '2024-02-29T14:09:05.569Z', 
        'type': 1, 
        'key': 'tasks.task:177', 
        'diff': {}, 
        'snapshot': None, 
        'values': {'users': {} }, 
        'values_diff': {}, 
        'comment': 'Experimentação para implementação da separação do módulo web em partes, administrativa e pública, testes e compatibilidades feitas e aguardando liberação para implementação. A implementação espera o aval para ser realizada, logo após o conserto de bugs no módulo.\n\n*   [x] Separação da área administrativa da área pública;\n*   [x] Separação da área pública da área administrativa;\n*   [x] Manutenção de rotas e domínios;\n*   [x] Limpeza de código e refatoração;\n*    [x]', 
        'comment_html': '<p>Experimentação para implementação da separação do módulo web em partes, administrativa e pública, testes e compatibilidades feitas e aguardando liberação para implementação. A implementação espera o aval para ser realizada, logo após o conserto de bugs no módulo.</p>\n<ul class="task-list">\n<li class="task-list-item"><label class="task-list-control"><input checked type="checkbox"><span class="task-list-indicator"></span></label> Separação da área administrativa da área pública;</li>\n<li class="task-list-item"><label class="task-list-control"><input checked type="checkbox"><span class="task-list-indicator"></span></label> Separação da área pública da área administrativa;</li>\n<li class="task-list-item"><label class="task-list-control"><input checked type="checkbox"><span class="task-list-indicator"></span></label> Manutenção de rotas e domínios;</li>\n<li class="task-list-item"><label class="task-list-control"><input checked type="checkbox"><span class="task-list-indicator"></span></label> Limpeza de código e refatoração;</li>\n<li>[x]</li>\n</ul>', 
        'delete_comment_date': None, 
        'delete_comment_user': None, 
        'edit_comment_date': '2024-03-06T19:35:34.890Z', 
        'is_hidden': False, 
        'is_snapshot': False
    }, 
    {
        'id': 'b0ef77dc-d6fd-11ee-8e02-2912dd486019', 'user': {'pk': 10, 'username': 'Adriana', 'name': 'Adriana Teles', 'photo': None, 'is_active': True, 'gravatar_id': 'b5dfe9fc6086b5e9c2cc22a22280d5ad'}, 'created_at': '2024-02-29T12:25:57.861Z', 'type': 1, 'key': 'tasks.task:177', 'diff': {'status': [72, 73], 'taskboard_order': [1708960241437, 0]}, 'snapshot': None, 'values': {'users': {}, 'status': {'72': 'New', '73': 'In progress'} }, 
        'values_diff': {'status': ['New', 'In progress'], 'taskboard_order': [1708960241437, 0]}, 'comment': '', 'comment_html': '', 'delete_comment_date': None, 'delete_comment_user': None, 'edit_comment_date': None, 'is_hidden': False, 'is_snapshot': False
    }, 
    {
        'id': '38bb5b5a-d4b9-11ee-8e02-2912dd486019', 'user': {'pk': 10, 'username': 'Adriana', 'name': 'Adriana Teles', 'photo': None, 'is_active': True, 'gravatar_id': 'b5dfe9fc6086b5e9c2cc22a22280d5ad'}, 'created_at': '2024-02-26T15:10:48.090Z', 'type': 1, 'key': 'tasks.task:177', 'diff': {'assigned_to': [None, 17]}, 'snapshot': None, 'values': {'users': {'17': 'Ronald José Gama Almeida'} }, 'values_diff': {'assigned_to': [None, 'Ronald José Gama Almeida']}, 'comment': '', 'comment_html': '', 'delete_comment_date': None, 'delete_comment_user': None, 'edit_comment_date': None, 'is_hidden': False, 'is_snapshot': False
    }
]
