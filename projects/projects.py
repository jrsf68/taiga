import logging

from repository.repository import insert_entity_into_repository, read_entity_from_repository


async def insert_project(project):
    result = await insert_entity_into_repository(entity='projects', rows=project)
    return result


async def read_projects(filter):
    projects = await read_entity_from_repository(entity='projects', filter=filter)
    logging.critical(projects)
    return projects
