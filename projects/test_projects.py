from projects.projects import insert_project, read_projects
import pytest


@pytest.mark.asyncio
async def test_insert_project():
    project = {
        'tool_id': 'taiga_x15',
        'tool': 'taiga',
        'id': 'x15',
        'name': 'Projeto Evolucionário',
        'description': 'Novidade na área',
        'modified_date': '2024-03-20 19:30:12',
    }
    result = await insert_project(project)
    return result

# @pytest.mark.asyncio
# async def test_read_projects():
#     filter = "all"
#     projects = await read_projects(filter)
#     assert projects or projects == []
