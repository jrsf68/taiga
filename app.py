import logging
import os
from pathlib import Path

from fastapi import FastAPI

import configuration.settings as settings

if not settings.vault_authentication():
    exit(0)
if not settings.check_env_vars():
    exit(0)

file_name = f'{str(Path(__file__).resolve().parent).split(os.sep)[-1]}_{str(Path(__file__).resolve()).split(os.sep)[-1].split(".")[0]}'
settings.cofig_logging(file_name)
logger = logging.getLogger(file_name)
app = FastAPI()


@app.get("/")
async def root():
    return {"message": "It's working!"}
