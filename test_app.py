"""Testing app.py
"""

import pytest

from app import root


@pytest.mark.asyncio
async def test_root():
    health = {"message": "It's working!"}
    result = await root()
    assert result == health
