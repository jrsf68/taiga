# Application configuration

## Migrations

```bash
poetry add alembic
mkdir repository/database/  # make sure you create 'repository/database/' folders
alembic init alembic
cp -f alembic.ini alembic_template.ini
# In file alembic/env.py, type: target_metadata = Base.metadata  # Import Base.
sed -i -e "s/from alembic import context/from alembic import context\n\nfrom domain.model import Base/g" alembic/env.py
sed -i -e "s/target_metadata = None/target_metadata = Base.metadata/g" alembic/env.py
# The first time, run: 
./alembic_migrations.sh initial
# Otherwise, run: 
./alembic_migrations.sh "reason_for_modification"
# Edit generated file in 'alembic/versions' folder and insert name to PrimaryKeyConstraint and UniqueConstraint.
# Example: sa.UniqueConstraint('tool_id', name='project_tool_id')
alembic upgrade head
```

