"""Test taiga functions.
"""
from os import getenv
import pytest

from taiga.taiga import get_taiga_new_projects


@pytest.mark.asyncio
async def test_get_taiga_new_projects():
    server = {
        'base_url': getenv('TAIGA_BASE_URL', ''),
        'username': getenv('TAIGA_USERNAME', ''),
        'password': getenv('TAIGA_PASSWORD', ''),
    }
    projects = []
    new_projects = await get_taiga_new_projects(server, projects)
    assert new_projects or new_projects == []


# @pytest.mark.asyncio
# async def test_get_developers():
#     """Get issues from taiga tool"""
#     server = {
#         'base_url': getenv('TAIGA_BASE_URL', ''),
#         'username': getenv('TAIGA_USERNAME', ''),
#         'password': getenv('TAIGA_PASSWORD', ''),
#     }
#     logging.critical('server: %s', server)
#     token_endpoint = f"{server['base_url']}/auth"
#     credential = {
#         'type': 'normal',
#         'username': server['username'],
#         'password': server['password'],
#     }
#     response = requests.post(token_endpoint, data=credential)
#     if response.status_code == 200:
#         access_token = response.json()['auth_token']
#         headers = {'Authorization': f'Bearer {access_token}'}
#         page = 0
#         while True:
#             page += 1
#             endpoint = f"{server['base_url']}/users?page_size=10&page={page}"
#             response = requests.get(endpoint, headers=headers)
#             users = response.json()
#             if response.status_code != 200 or not users:
#                 break
#             for user in users:
#                 if 'odrigo' in user['username']:
#                     logging.critical('xuser: %s', user)
#     assert False


# @pytest.mark.asyncio
# async def test_get_taiga_issues():
#     server = {
#         'base_url': getenv('TAIGA_BASE_URL', ''),
#         'username': getenv('TAIGA_USERNAME', ''),
#         'password': getenv('TAIGA_PASSWORD', ''),
#     }
#     projects = [{'id': 17, 'last_read_date': '2024-02-01T00:00:00'},
#                 {'id': 15, 'last_read_date': '2024-02-01T00:00:00'},]
#     developers = [
#         {'full_name': 'GABRIEL VIANNA SOARES ROCHA', 'taiga_id': 30},
#         {'full_name': 'JORGE ROGÉRIO PINHEIRO LOBO FILHO', 'taiga_id': 35},
#         {'full_name': 'JOSÉ RICARDO DA SILVA FERREIRA', 'taiga_id': 33},
#         {'full_name': 'RONALD JOSÉ GAMA ALMEIDA', 'taiga_id': 17},
#     ]
#     issues = await get_taiga_issues(server, projects, developers)
#     assert issues
