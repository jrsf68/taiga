"""Taiga tool fuctions.
"""
from datetime import datetime
import logging
import requests
import uuid

from configuration.create_id import create_id


async def taiga_connection(server):
    token_endpoint = f"{server['base_url']}/auth"
    credential = {
        'type': 'normal',
        'username': server['username'],
        'password': server['password'],
    }
    response = requests.post(token_endpoint, data=credential)
    if response.status_code == 200:
        access_token = response.json()['auth_token']
        return access_token
    else:
        return False


async def get_taiga_new_projects(server, projects):
    """Get projects from taiga tool."""
    new_projects = []
    access_token = await taiga_connection(server=server)
    if access_token:
        headers = {'Authorization': f'Bearer {access_token}'}
        page = 0
        while True:
            page += 1
            endpoint = f"{server['base_url']}/projects?page={page}"
            response = requests.get(endpoint, headers=headers)
            if response.status_code != 200:
                break
            new_projects.extend(
                [
                    {
                        "id": project['id'],
                        "name": project['name'],
                        "description": project['description'],
                        "modified_date": project['modified_date'],
                    }
                    for project in response.json()
                    if project['id'] not in projects
                ]
            )
            logging.debug('projects: %s', new_projects)
    return new_projects


async def get_taiga_issues(server, projects, developers):
    """Get issues from taiga tool"""
    token_endpoint = f"{server['base_url']}/auth"
    credential = {
        'type': 'normal',
        'username': server['username'],
        'password': server['password'],
    }
    response = requests.post(token_endpoint, data=credential)
    if response.status_code == 200:
        access_token = response.json()['auth_token']
        headers = {'Authorization': f'Bearer {access_token}'}
        endpoint = f"https://taiga.prodepa.pa.gov.br/api/v1/history/task/177"
        response = requests.get(endpoint, headers=headers)
        logging.debug(f'response.status_code: {response.status_code}')
        task = response.json()
        logging.debug(f'task: {task}')
        exit(0)

        offset = 0
        limit = 1000
        page = 0
        start_date = "2024-02-27"
        end_date = "2024-02-27"
        while page < 2:
            page += 1
            endpoint = f"https://taiga.prodepa.pa.gov.br/api/v1/comment/{page}?task=177"
            response = requests.get(endpoint, headers=headers)
            if response.status_code != 200:
                continue
            task = response.json()
            if task['subject'] == 'Separação do Módulo WEB: Público e Administrativo':
                logging.debug(f'response.status_code: {response.status_code}')
                logging.debug(f'task: {task}')
                 

        # all_tasks = []
        # for project in projects:
        #     while True:
        #         page += 1
        #         # endpoint = f"{server['base_url']}/tasks?project={project['id']}&page={page}"
        #         # endpoint = f"{server['base_url']}/projects"
        #         # endpoint = f"{server['base_url']}/projects/{project['id']}"
        #         # endpoint = f"{server['base_url']}/tasks/94"
        #         # endpoint = f"{server['base_url']}/tasks?project={project['id']}&offset={offset}&limit={limit}"
        #         # endpoint = f"{server['base_url']}/issues?project={project['id']}"
        #         # endpoint = f"{server['base_url']}/issues?modified_date__range={start_date},{end_date}"
        #         endpoint = f"https://taiga.prodepa.pa.gov.br/api/v1/tasks/87"
        #         response = requests.get(endpoint, headers=headers)
        #         logging.debug(response.status_code)
        #         if response.status_code != 200:
        #             break
        #         tasks = response.json()
        #         logging.debug(f'tasks x: {tasks}')
        #         # for task in tasks:
        #         #     logging.debug(f'task: {json.dumps(task)}')
        #         #     logging.debug(f'=================')
        #         logging.debug(f'================= FIM')
        #         logging.debug(f'================= FIM')
        #         all_tasks.extend(
        #             [
        #                 {
        #                     '_id': str(uuid.uuid4()),
        #                     'tool': 'taiga',
        #                     'developer_full_name': [
        #                         developer['full_name']
        #                         for developer in developers
        #                         if developer['taiga_id']
        #                         == (
        #                             task['owner_extra_info']['id']
        #                             if task['assigned_to'] == None
        #                             else task['assigned_to_extra_info']['id']
        #                         )
        #                     ][0],
        #                     'developer_email': '',
        #                     'developer_name': task['owner_extra_info']['username']
        #                     if task['assigned_to'] == None
        #                     else task['assigned_to_extra_info']['username'],
        #                     'project': task['project_extra_info']['name'],
        #                     'task_id': str(task['id']),
        #                     'task': f'#{task["ref"]} - {task["subject"]} {task["status_extra_info"]["name"]}',
        #                     'complement': '',
        #                     'completion_date': datetime.fromisoformat(
        #                         task['modified_date']
        #                     ).strftime('%Y-%m-%d %H:%M:%S'),
        #                     'project_date': project['last_read_date'],
        #                     'status': task['status_extra_info']['name'],
        #                 } 
        #                 for task in tasks
        #                 if 1==1
        #             ]
        #         )
        #         logging.debug(f'all_tasks: {all_tasks}')
        #         if len(tasks) < limit:
        #             break
        #         else:
        #             offset += limit
    else:
        logging.debug('Error occurred:', response.text)
    return False
