"""Envirinment configuration funtions.
"""

import json
import logging
import logging.handlers
import os
from pathlib import Path as create_dir 
import configuration.vault as vault


def check_env_vars():
    """checks if all environment variables exist."""
    app_env_vars = []
    try:
        with open('configuration/settings.json') as setting_file:
            app_setting_data = setting_file.read()
        setting_data_json = json.loads(app_setting_data)
        for _, value in setting_data_json.items():
            if 'env_vars' in value:
                app_env_vars.extend(
                    list(value['env_vars'].keys())
                )
        if not app_env_vars:
            logging.critical('missing env_vars.')
            exit(0)
    except FileNotFoundError as e:
        logging.critical('missing settings.json file: %s', e)
        exit(0)
    except TypeError as e:
        logging.critical('missing data in settings.json: %s', e)
        exit(0)
    except KeyError as e:
        logging.critical('missing env_vars key: %s', e)
        exit(0)
    # Load values to environment.
    for _, value in setting_data_json.items():
        if 'env_vars' in value:
            for env_var, env_var_value in value['env_vars'].items():
                if env_var_value:
                    os.environ[env_var] = env_var_value
    # Load secrets from vault.
    try:
        secrets = vault.reading_a_secret(vault_authentication())
        for secret in secrets:
            os.environ[secret] = secrets[secret]
    except Exception as e:
        logging.critical('Error reading secrets from Vault (%s)', e)
        return False
    # checks if all environment variables exists.
    environment = list(vars(os.environ)['_data'].keys())
    missing_env_var = [
        var 
        for var in app_env_vars 
        if var not in environment and var not in ['DBMS_DRIVE', 'DATABASE_PATH']
    ]
    if missing_env_var:
        logging.critical('missing environment variable: %s', missing_env_var)
        return False
    return True


def vault_authentication():
    VAULT_ADDR = os.getenv('VAULT_ADDR', 'VAULT_ADDR env var not found.') 
    VAULT_TOKEN = os.getenv('VAULT_TOKEN', 'VAULT_TOKEN env var not found')
    try:
        client = vault.authentication(VAULT_ADDR, VAULT_TOKEN)
    except ValueError as e:
        logging.critical('Vault authentication error (%s).', e)
        return False
    if VAULT_TOKEN == 'VAULT_TOKEN env var not found':
        logging.critical('Vault authentication error (%s).', VAULT_TOKEN)
        return False
    if not client:
        logging.critical('Vault authentication error.')
        return False
    return client


def cofig_logging(file_name):
    create_dir('logs').mkdir(exist_ok=True)
    LOGGING_LEVEL = os.getenv('LOGGING_LEVEL', 'ERROR')
    handler = logging.handlers.RotatingFileHandler(
        f'logs/{file_name}.log',
        maxBytes=int(1024*1024*float(os.getenv('LOGGING_MAX_MEGA_BYTES', '0.001'))),
        backupCount=int(float(os.getenv('LOGGING_BACKUP_COUNT', '2')))
    )
    logging.basicConfig(
        level=LOGGING_LEVEL,
        datefmt= '%Y-%m-%dT%H:%M:%SZ',
        format='{"timestamp": "%(asctime)s", "logger": "%(name)s", "level": "%(levelname)s", "message": "%(message)s"}',
        handlers=[handler]
    )
