from datetime import datetime

def create_id():
    current_time = datetime.now()
    timestamp = current_time.timestamp()
    _id = f'{int(timestamp):x}{int((timestamp-int(timestamp))*100000):x}'.ljust(24, '0')
    return _id
