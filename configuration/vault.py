"""secret management module functions.
"""
import logging
from urllib.request import urlopen
from urllib.error import HTTPError, URLError

import hvac
import hvac.exceptions


def authentication(url, token):
    try:
        urlopen(url=url)
        client = hvac.Client(url, token)
    except HTTPError as e:
        logging.critical('HTTP error: (%s)', e)
        return False
    except URLError as e:
        logging.critical('Vault server not found: (%s)', e)
        return False
    if client.seal_status['sealed']:
        logging.critical('Vault is sealed.')
        return False
    if not client.is_authenticated():
        logging.critical('Vault authentication failed.')
        return False
    return client


# def writing_a_secret(client):
#     create_response = client.secrets.kv.v2.create_or_update_secret(
#         path='my-secret-password',
#         secret=dict(password='Hashi123'),
#     )
#     return create_response


def reading_a_secret(client):
    read_response = client.secrets.kv.read_secret_version(path='userpass', raise_on_deleted_version=True)
    try:
        sercrets = read_response['data']['data']
    except hvac.exceptions.VaultError as e:
        logging.critical('Secrets not found on Vault Server (%s)', e)
        return {}
    except KeyError as e:
        logging.critical('Secrets not found on Vault Server (%s)', e)
        return {}
    return sercrets
