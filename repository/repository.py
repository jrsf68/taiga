import logging
from os import getenv

from repository.sqlite_functions import insert_entity_into_sqlite, read_entity_from_sqlite


async def insert_entity_into_repository(entity, rows):
    if getenv('DBMS', '') == 'sqlite3':
        result = await insert_entity_into_sqlite(entity=entity, rows=rows)
        return result
    return False


async def read_entity_from_repository(entity, filter):
    if getenv('DBMS', '') == 'sqlite3':
        projects = await read_entity_from_sqlite(entity=entity, filter=filter)
        return projects
    return False
