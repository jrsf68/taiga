from datetime import datetime
import logging
from os import getenv
from pandas import DataFrame, read_sql_query
from sqlite3 import connect, OperationalError
from configuration.create_id import create_id
import configuration.settings as settings

if not settings.vault_authentication():
    exit(0)
if not settings.check_env_vars():
    exit(0)


DB_STRING_CONNETION = getenv('DATABASE_PATH', '')
DB_STRING_CONNETION += '/' + getenv('DATABASE_NAME', '')
DB_STRING_CONNETION += '.db'


async def database_connection():
    try:
        conn = connect(DB_STRING_CONNETION)
        return conn
    except ConnectionError as e:
        logging.error('`%s` connection error: (%s)', DB_STRING_CONNETION, e)
    except OperationalError as e:
        logging.error('`%s` connection error: (%s)', DB_STRING_CONNETION, e)
    return False


async def create_entity_into_sqlite(entity):
    conn = await database_connection()
    # if conn:
    #     try:


async def insert_entity_into_sqlite(entity, rows):
    conn = await database_connection()
    if conn:
        try:
            data_frame = DataFrame([rows])
            data_frame.insert(0, '_id', create_id())
            data_frame.insert(1, 'insert_at', datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            data_frame.to_sql(name=entity, con=conn, if_exists='append', index=False)
            conn.close()
            return True
        except Exception as e:
            logging.error('`%s` entity insert error.', entity)
    return False


async def read_entity_from_sqlite(entity: str, filter: str = '') -> list:
    filter_sentence = '' if not filter or filter.upper() == 'ALL' else 'WHERE ' + filter
    query = f'SELECT * FROM {entity} {filter_sentence};'
    conn = await database_connection()
    if conn:
        try:
            df = read_sql_query(query, conn)
            rows = df.to_dict(orient='records')
            conn.close()
            return rows
        except Exception as e:
            logging.error('`%s` entity access error.', entity)
    return False
