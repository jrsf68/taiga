# make sure you create 'repository/database/' folders
# The first time, run: ./alembic_migrations.sh initial
# Otherwise, run: ./alembic_migrations.sh <reason_for_modification>
DBMS=$(grep -0 '"DBMS": .*' configuration/settings.json | cut -d: -f2 | cut -d'"' -f 2)
DBMS_DRIVE=$(grep -0 '"DBMS_DRIVE": .*' configuration/settings.json | cut -d: -f2 | cut -d'"' -f 2)
DB_USERNAME=$(grep -0 '"DB_USERNAME": .*' configuration/settings.json | cut -d: -f2 | cut -d'"' -f 2)
DB_PASSWORD=$(vault kv get -field="data" -mount="task_management_projects" "userpass" | grep -0 'DB_PASSWORD:*'  | cut -d" " -f1 | cut -d: -f2)
DB_HOSTNAME=$(grep -0 '"DB_HOSTNAME": .*' configuration/settings.json | cut -d: -f2 | cut -d'"' -f 2)
DB_PORT=$(grep -0 '"DB_PORT": .*' configuration/settings.json | cut -d: -f2 | cut -d'"' -f 2)
DATABASE_PATH=$(grep -0 '"DATABASE_PATH": .*' configuration/settings.json | cut -d: -f2 | cut -d'"' -f 2)
DATABASE_NAME=$(grep -0 '"DATABASE_NAME": .*' configuration/settings.json | cut -d: -f2 | cut -d'"' -f 2)
cp alembic_template.ini alembic.ini
if [ $DBMS = 'sqlite' ]; then
    # sqlalchemy.url = driver:///dbpath/dbname.db
    CONNECTION_STRING=$"$DBMS:///$DATABASE_PATH/$DATABASE_NAME.db"
elif [ -z "$DBMS_DRIVE" ]; then
    # sqlalchemy.url = driver://user:pass@localhost/dbname
    CONNECTION_STRING=$"$DBMS://$DB_USERNAME:$DB_PASSWORD@$DB_HOSTNAME:$DB_PORT/$DATABASE_NAME"
else
    # sqlalchemy.url = mysql+pymysql://<db_user>:<db_pass>@<db_host>:<db_port>/<db_name>
    CONNECTION_STRING=$"$DBMS+$DBMS_DRIVE://$DB_USERNAME:$DB_PASSWORD@$DB_HOSTNAME:$DB_PORT/$DATABASE_NAME"
fi
awk -v var_DATABASE_PATH="$CONNECTION_STRING" '{sub("driver://user:pass@localhost/dbname", var_DATABASE_PATH)}1' alembic_template.ini > alembic.ini
sed -i -e "s/driver\:\/\/user\:pass@localhost\/dbname/\&/g" alembic.ini
if [ "$1" = 'initial' ]; then
    alembic revision --autogenerate -m "initial"
elif [ -z "$1" ]; then
    alembic revision --autogenerate -m "database_modification"
else
    alembic revision --autogenerate -m $1
fi
